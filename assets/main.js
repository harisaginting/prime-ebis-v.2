jQuery.validator.addMethod("checkPassword", function(value, element)
 {
     var response;
     var url      = base_url + "user/check-password";
     
         $.ajax({
                type: "POST",
                url: url,
                data: {password : value, id : $("#userid").val()},
                dataType: "text",
                async : false
         }).done(function(result){
              if(result.trim() == "1"){
                  response = true;
              }else{
                  response = false;    
              }
              console.log(result);
              console.log(response);
         });
         return response;
 }, "Your Current Password not match");


jQuery.validator.addMethod("checkUserId", function(value, element)
{
     var response;
     var url      = base_url + "v/check-user-id";
     
         $.ajax({
                type: "POST",
                url: url,
                data: {id : value},
                dataType: "text",
                async: false
         }).done(function(result){
             console.log(result);
              if(result.trim() == "1"){
                  response = true;
              }else{
                  response = false;    
              }
         });
         return response;
 }, "This ID has been used");


jQuery.validator.addMethod("checkUserIdReverse", function(value, element)
{
     var response;
     var url      = base_url + "v/check-user-id";
     
         $.ajax({
                type: "POST",
                url: url,
                data: {id : value},
                dataType: "text",
                async: false
         }).done(function(result){
             console.log(result);
              if(result.trim() == "1"){
                  response = false;
              }else{
                  response = true;    
              }
         });
         return response;
 }, "This ID not registered");

jQuery.validator.addMethod("checkUserEmail", function(value, element)
{
     var response;
     var url      = base_url + "v/check-user-email";
     
         $.ajax({
                type: "POST",
                url: url,
                data: {email : value},
                dataType: "text",
                async: false
         }).done(function(result){
             console.log(result);
              if(result.trim() == "1"){
                  response = true;
              }else{
                  response = false;    
              }
         });
         return response;
 }, "This Email has been used");


jQuery.validator.addMethod("checkUserEmailReverse", function(value, element)
{
     var response;
     var url      = base_url + "v/check-user-email";
     
         $.ajax({
                type: "POST",
                url: url,
                data: {email : value},
                dataType: "text",
                async: false
         }).done(function(result){
             console.log(result);
              if(result.trim() == "1"){
                  response = false;
              }else{
                  response = true;    
              }
         });
         return response;
 }, "This Email not registered in PRIME");


jQuery.extend(jQuery.validator.messages, {
                equalTo: jQuery.validator.format("Make Sure Have same Value")
            });  

$('.form-validator').validate({
            errorElement: 'span',
            errorClass: 'text-danger',
            highlight: function(element) {
                $(element).closest('.form-group').addClass('errors');
            },
            unhighlight: function(element) {
                $(element).closest('.form-group').removeClass('error');
            },
            success: function(element){
                $(element).closest('.form-group').removeClass('error');
            },
            errorPlacement: function(error, element) {
                if (element.hasClass('select2-hidden-accessible')) {
                    error.insertAfter(element.next('span')); 
                } else {
                    error.insertAfter(element);            
                }
            },
            submitHandler: function(form) {
                form.submit();
            }
        }); 