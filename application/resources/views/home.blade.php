
<!DOCTYPE html>

<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
  <meta name="description" content="PROJECT APPLICATION MANAGEMENT">
  <title>PRIME</title>
  <!-- Icons-->
    <link href="{{url('/')}}/assets/template/coreui/node_modules/@coreui/icons/css/coreui-icons.min.css" rel="stylesheet">
    <link href="{{url('/')}}/assets/template/coreui/node_modules/flag-icon-css/css/flag-icon.min.css" rel="stylesheet">
    <link href="{{url('/')}}/assets/template/coreui/node_modules/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <link href="{{url('/')}}/assets/template/coreui/node_modules/simple-line-icons/css/simple-line-icons.css" rel="stylesheet">
    <link href="{{url('/')}}/assets/template/coreui/src/css/style.css" rel="stylesheet">
    <link href="{{url('/')}}/assets/template/coreui/src/vendors/pace-progress/css/pace.min.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Catamaran:400,700,800,900&display=swap" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="{{url('/')}}/assets/css/login.css?v=2">
  <!-- CoreUI and necessary plugins-->
    <script src="{{url('/')}}/assets/template/coreui/node_modules/jquery/dist/jquery.min.js"></script>
    <script src="{{url('/')}}/assets/template/coreui/node_modules/popper.js/dist/umd/popper.min.js"></script>
    <script src="{{url('/')}}/assets/template/coreui/node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="{{url('/')}}/assets/template/coreui/node_modules/pace-progress/pace.min.js"></script>
    <script src="{{url('/')}}/assets/template/coreui/node_modules/perfect-scrollbar/dist/perfect-scrollbar.min.js"></script>
    <script src="{{url('/')}}/assets/template/coreui/node_modules/@coreui/coreui/dist/js/coreui.min.js"></script>
</head>
<body class="app flex-row align-items-center">
    <div class="container">
      <div class="row justify-content-center">
        <div class="col-md-8">
          <div class="card-group">
            <div class="card p-4 container_form_login" style="border-radius:20px;">
              <div class="card-body">
                <form action="{{url('/')}}login/login_proccess" method="POST" enctype="multipart/form-data">
                  <div class="mb-3">
                    <div style="width: 100%;text-align: center;">
                      <span class="label-prime">PRIME</span>
                    </div>
                    <div style="width: 100%;text-align: center;margin-top: -25px;">
                      <span style="letter-spacing: 0.7em;font-size: 0.8em;font-weight: 800;">PROJECT MANAGEMENT</span>
                    </div>
                  </div>
                  <div class="input-group mb-3 mt-4 pl-4 pr-4">
                    <div class="input-group-prepend">
                    <span class="input-group-text">
                    <i class="icon-user"></i>
                    </span>
                    </div>
                    <input class="form-control" type="text" name="username" placeholder="Username" required>
                    </div>
                  <div class="input-group mb-4 pl-4 pr-4">
                    <div class="input-group-prepend">
                      <span class="input-group-text">
                    <i class="icon-lock"></i>
                    </span>
                    </div>
                    <input name="password" class="form-control" type="password" placeholder="Password" required>
                  </div>
                  <div class="row">
                    <div class="col-12">
                    </div>
                  </div>
                  <div class="row text-center">
                      <a href="{{url('/')}}forget-password" class="btn btn-disabled btn-md" style="margin: auto;" type="button"><small>FORGET PASSWORD ?</small></a>
                      <button class="btn btn-primary btn-md" style="margin: auto;" type="submit"><span>&nbsp;&nbsp;LOGIN&nbsp;&nbsp;&nbsp;</span></button>
                  </div>
                </form>
              </div>
            </div>
            <div class="card text-white py-5 d-md-down-none container_remarks" style="width:44%;border-top-right-radius: 20px;border-bottom-right-radius:20px;margin-left: -20px;z-index: -20">
            <div class="card-body text-center">
            <div>
            <img src="{{url('/')}}/assets/img/thumb-telkom.png" alt="" width="180" class='mt-3'/>
            </div>
            </div>
            <span class="footprint"><span style="color:#28388a">DBS</span> - <span style="color:#ff7100">DES</span> - <span style="color:#198000">DGS</span></span>
            </div>
          </div>

                <!-- <div class="w-100" style="padding-left: 20px;">
                    demo account : 
                    <div class="row">
                      <div class="col-md-4 col-sm-12 bg-white border rounded-lg">
                        <div class="row border border-bottom-2">
                          <div class="col-2">EBIS</div>
                          <div class="col-5 text-right">username<br>password</div>
                          <div class="col-5"><strong>admin<br>password</strong></div>
                        </div>
                        <div class="row border border-bottom-2">
                          <div class="col-2">DBS</div>
                          <div class="col-5 text-right">username<br>password</div>
                          <div class="col-5"><strong>admin_dbs<br>password</strong></div>
                        </div>
                        <div class="row border border-bottom-2">
                          <div class="col-2">DES</div>
                          <div class="col-5 text-right">username<br>password</div>
                          <div class="col-5"><strong>admin_des<br>password</strong></div>
                        </div>
                        <div class="row">
                          <div class="col-2">DGS</div>
                          <div class="col-5 text-right">username<br>password</div>
                          <div class="col-5"><strong>admin_dgs<br>password</strong></div>
                        </div>
                      </div>
                    </div>
                </div> -->
        
        </div>
      </div>
    </div>

</body>
</html>