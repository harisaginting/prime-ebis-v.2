<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


// LANDING PAGE
Route::get('/', 'LandingController@index')->name('home');
       // Route::get('/artikel/', 'LandingController@artikelList')->name('artikel-list');
       // Route::get('/artikel/{url_key}', 'LandingController@artikel')->name('artikel');


       // Route::get('/sejarah-gbkp/', 'LandingController@sejarah')->name('sejarah');

       // Route::post('dashboard/chart-total-kategorial', 'DashboardController@chartTotalKategorial')->name('frontend-chart-total-kategorial');